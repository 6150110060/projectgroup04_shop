package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class pointsystem extends AppCompatActivity implements View.OnClickListener{


    ImageView scan1,scan2,imageView4;
    Button buttoncommit2;
    TextView textViewpoint,pointadd,textView9;
    EditText idcutomer2,idcoupon2,amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pointsystem);

        setTitle("สะสมแต้ม");

        scan1 = findViewById(R.id.scan1);
        scan1.setOnClickListener(this);

        idcutomer2 = findViewById(R.id.idcustomer2);
        idcoupon2 = findViewById(R.id.idcoupon2);
        amount = findViewById(R.id.amount);

        scan2 = findViewById(R.id.scan2);
        scan2.setOnClickListener(this);

        imageView4 = findViewById(R.id.imageView4);
        textViewpoint = findViewById(R.id.textViewpoint);
        pointadd = findViewById(R.id.pointadd);
        textView9 = findViewById(R.id.textView9);


        buttoncommit2 = findViewById(R.id.buttoncommit2);
        buttoncommit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!idcutomer2.getText().toString().isEmpty()&&!amount.getText().toString().isEmpty()) {
                int point = Integer.valueOf(amount.getText().toString())/20;
                pointadd.setText(String.valueOf(point));;
                imageView4.setVisibility(View.VISIBLE);
                textViewpoint.setVisibility(View.VISIBLE);
                pointadd.setVisibility(View.VISIBLE);
                textView9.setVisibility(View.VISIBLE);
                idcoupon2.getText().clear();
                idcutomer2.getText().clear();
                amount.getText().clear();
                }
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.point);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:


                        break;
                    case R.id.icon4:

                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.option:

                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });
    }



    @Override
    public void onClick(View v) {
        scanCode();
    }
    private void scanCode(){

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(CaptureAct.class);
        integrator.setOrientationLocked(false);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES);
        integrator.setPrompt("Scanning Code");
        integrator.initiateScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null){
            if (result.getContents() !=null){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(result.getContents());
                builder.setTitle("Scanning Result");
                builder.setPositiveButton("Scan Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scanCode();
                    }
                }).setNegativeButton("finish", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {
                Toast.makeText(this, "No Results", Toast.LENGTH_LONG).show();
            }
        }else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}