package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class editcoupon extends AppCompatActivity {

    String idshop = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editcoupon);

        setTitle("แก้ไขส่วนลดร้านค้า");

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.discount);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:

                        startActivity(new Intent(getApplicationContext(),pointsystem.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.icon4:

                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.option:

                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });
    }

    public void idshop(){
        SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
        idshop = preferences.getString("iduser2", "14");
    }

}