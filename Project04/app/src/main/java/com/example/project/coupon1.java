package com.example.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabItem;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class coupon1 extends Fragment {

    Systeminterface systeminterface;

    String idshop = "14";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public coupon1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment coupons1.
     */
    // TODO: Rename and change types and number of parameters
    public static coupon1 newInstance(String param1, String param2) {
        coupon1 fragment = new coupon1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.activity_coupon1, container, false);
        ImageView imageView = view.findViewById(R.id.discount10);
        ImageView imageView20 = view.findViewById(R.id.discount20);
        ImageView imageView50 = view.findViewById(R.id.discount50);
        ImageView imageView100 = view.findViewById(R.id.discount100);
        ImageView imageView400 = view.findViewById(R.id.discount400);




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.myBASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);



        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addcoupon("1","10","10");
            }
        });

        imageView20.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addcoupon("2","20","20");
            }
        });

        imageView50.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addcoupon("3","50","50");
            }
        });

        imageView100.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addcoupon("4","100","100");

            }
        });

        imageView400.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                addcoupon("5","400","400");

            }
        });

        return view;
    }

    public void  addcoupon(String idcoupon, String count, String point){

        Call<loginresponse> call = systeminterface.addconpon(idshop,idcoupon,"ส่วนลด "+count+" บาท","Bath",count,point);
call.enqueue(new Callback<loginresponse>() {
    @Override
    public void onResponse(Call<loginresponse> call, Response<loginresponse> response) {

        if (response.body().getSuccess().equals("true2")) {
            Intent i = new Intent(getContext(),coupons.class);
            startActivity(i);
        }
        else {

            Toast.makeText(getContext(), "ส่วนลดนี้ได้เพิ่มแล้ว", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onFailure(Call<loginresponse> call, Throwable t) {

    }
});
    }


}