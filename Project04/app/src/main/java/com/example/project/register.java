package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class register extends AppCompatActivity {

    Button register;
    EditText username, password, name, shopname, phonenumber, email, address;
    CheckBox checkBox2;

    Systeminterface systeminterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        setTitle("ลงทะเบียนผู้ใช้งานใหม่");


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkBox2 = findViewById(R.id.checkBox2);
        Button next = findViewById(R.id.register);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                register();
            }
        });

        checkBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox2.isChecked()) {
                    Intent intent4 = new Intent(getApplicationContext(), checkbox.class);
                    startActivity(intent4);
                } else {

                }
            }
        });

        username = findViewById(R.id.name1);
        password = findViewById(R.id.name2);
        name = findViewById(R.id.name3);
        shopname = findViewById(R.id.name4);
        phonenumber = findViewById(R.id.name5);
        email = findViewById(R.id.name6);
        address = findViewById(R.id.name7);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);


    }

    public void register() {
        if (!username.getText().toString().isEmpty() && !password.getText().toString().isEmpty() &&
                !name.getText().toString().isEmpty() && !shopname.getText().toString().isEmpty() &&
                !phonenumber.getText().toString().isEmpty() && !email.getText().toString().isEmpty()
                && !address.getText().toString().isEmpty()) {
            if (checkBox2.isChecked()) {
                            Call<ResponseBody> responseBodyCall = systeminterface.registershop(username.getText().toString(),
                                    password.getText().toString(),
                                    name.getText().toString(),
                                    shopname.getText().toString(),
                                    phonenumber.getText().toString(),
                                    email.getText().toString(),
                                    address.getText().toString());
                            responseBodyCall.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    Intent i = new Intent(register.this, login.class);
                                    startActivity(i);
                                    Toast.makeText(getApplicationContext(), "Register", Toast.LENGTH_SHORT).show();
                                }
                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(getApplicationContext(), "" + t, Toast.LENGTH_SHORT).show();
                                }
                            });
                    }
            else {
                Toast.makeText(getApplicationContext(), "กรุณายอบรับเงื่อนไขและข้อตกลง", Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), "กรุณากรอกให้ครบทุกช่อง", Toast.LENGTH_SHORT).show();
        }
    }


}