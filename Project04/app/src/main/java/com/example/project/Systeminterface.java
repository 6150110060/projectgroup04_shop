package com.example.project;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Systeminterface {

    public static String BASE_URL = "https://itlearningcenters.com/android/project0407/";
    public static String myBASE_URL = "http://192.168.0.104/login/";

    //my API
    @FormUrlEncoded
    @POST("login.php")//
    Call<loginresponse> login(@Field("username") String user, @Field("password") String pass);

    @FormUrlEncoded
    @POST("register.php")//
    Call<ResponseBody> register(@Field("username") String userNameValue,
                                @Field("password") String passWordValue,
                                @Field("name") String NameValue,
                                @Field("shopname") String shopNameValue,
                                @Field("phonenumber") String phoneNumberValue,
                                @Field("email") String emailValue,
                                @Field("address") String addressValue);

    @FormUrlEncoded
    @POST("editpass.php")//
    Call<loginresponse> editpassword(@Field("id") String id,
                                     @Field("password") String PasswordValue
    );

    @FormUrlEncoded
    @POST("editprofile.php")//
    Call<loginresponse> editprofile(@Field("id") String id,
                                    @Field("name") String NameValue,
                                    @Field("shopname") String shopNameValue,
                                    @Field("phonenumber") String phoneNumberValue,
                                    @Field("email") String emailValue,
                                    @Field("address") String addressValue);

    @GET("view.php")//
    Call<List<loginresponse>> viewiduser(@Query("id") String id);


    @FormUrlEncoded
    @POST("addcoupon.php")//
    Call<loginresponse> addconpon(@Field("idshop") String idshop,
                                    @Field("idcoupon") String idcoupon,
                                    @Field("name") String name,
                                    @Field("type") String type,
                                    @Field("count") String count,
                                    @Field("point") String point);

    @GET("viewcouponshop.php")//
    Call<List<couponresponse>> viewcouponshop(@Query("id") String id);


    //API project0407
    @FormUrlEncoded
    @POST("regist_shop.php")//
    Call<ResponseBody> registershop(@Field("username_sh") String userNameValue,
                                    @Field("password_sh") String passWordValue,
                                    @Field("name_owner") String NameValue,
                                    @Field("name_shop") String shopNameValue,
                                    @Field("phone_no") String phoneNumberValue,
                                    @Field("email_sh") String emailValue,
                                    @Field("add_shop") String addressValue);

}
