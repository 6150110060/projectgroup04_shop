package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class customercustom extends BaseAdapter {

    private Context context;

    private String[] idcustomer;
    private String[] amount;
    private String[] point;
    private int[] imgcustomer;
    private LayoutInflater layoutInflater;


    public customercustom(Context applicationContext, String[] idcustomer, String[] amount,String[] point,int[] imgcustomer) {
        context = applicationContext;
        this.idcustomer = idcustomer;
        this.amount = amount;
        this.point = point;
        this.imgcustomer = imgcustomer;
    }

    @Override
    public int getCount() {
        return idcustomer.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.listcustomer,null);
        }

        ImageView imageView = view.findViewById(R.id.imageprofile);
        TextView textView = view.findViewById(R.id.idcustomer);
        TextView textView2 = view.findViewById(R.id.amount);
        TextView textView4 = view.findViewById(R.id.point);

        imageView.setImageResource(imgcustomer[i]);
        textView.setText(idcustomer[i]);
        textView2.setText(amount[i]);
        textView4.setText(point[i]);

        return view;
    }
}
