package com.example.project;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class login extends AppCompatActivity {

    Button login,register;
    EditText edituser,editpass;
    Systeminterface systeminterface;
    SharedPreferences preferences;
    CheckBox checkremember;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.buttonlogin);
        register = findViewById(R.id.buttonregister);
        edituser = findViewById(R.id.user);
        editpass = findViewById(R.id.pass);
        checkremember = findViewById(R.id.checkremember);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);

        preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
        edituser.setText(preferences.getString("username", null));
        editpass.setText(preferences.getString("password", null));
        checkremember.setChecked(preferences.getBoolean("check",false));

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),register.class));
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    public void login(){
        final String user = edituser.getText().toString();
        final String pass = editpass.getText().toString();

        if (user.equals("admin")||user.equals("")){
            startActivity(new Intent(getApplicationContext(),home.class));
        }
        if (user.isEmpty()||pass.isEmpty()) {

            Toast.makeText(getApplicationContext(), "Required field(s) is missing", Toast.LENGTH_SHORT).show();
        }
        else {
            Call<loginresponse> loginresponseCall = systeminterface.login(user,pass);
            loginresponseCall.enqueue(new Callback<loginresponse>() {
                @Override
                public void onResponse(Call<loginresponse> call, Response<loginresponse> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getSuccess().equals("true")){

                            saveidshop(response.body().getUserid());
                            startActivity(new Intent(getApplicationContext(),home.class));

                            Toast.makeText(getApplicationContext(), "Login", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "รหัสไม่ถูกต้อง กรุณากรอกรหัสใหม่", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<loginresponse> call, Throwable t) {

                }
            });
        }

    }

    public void saveidshop(String idshop){

//                        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("iduser2", idshop);
        edit.commit();
    }

    public void saveidpass(View view){
        SharedPreferences.Editor edit = preferences.edit();
        if (checkremember.isChecked()) {
            edit.putString("username", edituser.getText().toString());
            edit.putString("password", editpass.getText().toString());
            edit.putBoolean("check", true);
            edit.commit();
        } else {
            edit.clear();
            edit.commit();
        }
    }
}