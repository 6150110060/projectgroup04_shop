package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class customer extends AppCompatActivity {

    String[] idcustomer = {"1","10"};
    String[] amount = {"100","100"};
    String[] point = {"10","10"};
    int[] imgcustomer = {R.drawable.fsgsg,R.drawable.fsgsg};
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer);

        setTitle("ประวัติการสะสมแต้ม");

        listView = findViewById(R.id.listcustomer);
        customercustom customercustom = new customercustom(getApplicationContext(),idcustomer,amount,point,imgcustomer);
        listView.setAdapter(customercustom);

        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),pointsystem.class));
                overridePendingTransition(0,0);
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.icon4);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.icon4:


                        break;
                    case R.id.option:

                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });


    }
}