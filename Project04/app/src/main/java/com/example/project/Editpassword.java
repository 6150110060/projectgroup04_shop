package com.example.project;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Editpassword extends AppCompatActivity {

    EditText editpass,editpassnew,editpassnew2;
    Button buttoncommit,buttoncancel;
    Systeminterface systeminterface;
    String userpassword;
    String idshop = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editpassword);

        ActionBar actionBar= getSupportActionBar();
        setTitle("แก้ไขรหัสผ่าน");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editpass = findViewById(R.id.editpass);
        editpassnew = findViewById(R.id.editpassnew);

        editpassnew2 = findViewById(R.id.editpassnew2);
        buttoncommit = findViewById(R.id.buttoncommit);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.myBASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);

        idshop();

        buttoncommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editpassword();
            }
        });

        buttoncancel = findViewById(R.id.buttoncancel);
        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editpass.getText().clear();
                editpassnew.getText().clear();
                editpassnew2.getText().clear();
            }
        });
    }

    public void editpassword(){

        Call<List<loginresponse>> call = systeminterface.viewiduser(idshop);
        call.enqueue(new Callback<List<loginresponse>>() {
            @Override
            public void onResponse(Call<List<loginresponse>> call, Response<List<loginresponse>> response) {
                userpassword = response.body().get(0).getPassword();
                if (userpassword.equals(editpass.getText().toString())) {
                    if (editpassnew.getText().toString().isEmpty()){
                        Toast.makeText(getApplicationContext(), "กรุณาใส่รหัสผ่านใหม่", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (editpassnew.getText().toString().equals(editpassnew2.getText().toString())) {
                            Call<loginresponse> editpassword = systeminterface.editpassword(idshop, editpassnew.getText().toString());
                            editpassword.enqueue(new Callback<loginresponse>() {
                                @Override
                                public void onResponse(Call<loginresponse> call, Response<loginresponse> response) {
                                    Toast.makeText(getApplicationContext(), "edit password", Toast.LENGTH_SHORT).show();

                                }

                                @Override
                                public void onFailure(Call<loginresponse> call, Throwable t) {

                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "รหัสผ่านไม่ตรงกัน", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    Toast.makeText(getApplicationContext(), "รหัสผ่านเก่าไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<loginresponse>> call, Throwable t) {

            }
        });




    }

    public void idshop(){
        SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
        idshop = preferences.getString("iduser2", "14");


    }

}