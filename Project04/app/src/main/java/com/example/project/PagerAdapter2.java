package com.example.project;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class PagerAdapter2 extends FragmentStatePagerAdapter {

    int count;

    public PagerAdapter2(@NonNull FragmentManager fm,int count) {
        super(fm);
        this.count = count;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position)
        {
            case 0:
                fragment = new coupon1();
break;
            case 1:
                fragment = new coupon2();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return count;
    }
}
