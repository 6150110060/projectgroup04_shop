package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Optioncustom extends BaseAdapter {

    private Context context;

    private String[] option;
    private int[] imgoption;
    private int[] imgicon;
    private LayoutInflater layoutInflater;


    public Optioncustom(Context applicationContext, String[] option, int[] imgoption,int[] imgicon) {
        context = applicationContext;
        this.option = option;
        this.imgoption = imgoption;
        this.imgicon = imgicon;
    }

    @Override
    public int getCount() {
        return imgoption.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.listoptionmain,null);
        }

        ImageView imageView = view.findViewById(R.id.customoption);
        TextView textView = view.findViewById(R.id.customoption2);

        imageView.setImageResource(imgoption[i]);
        textView.setText(option[i]);

        ImageView imageView2 = view.findViewById(R.id.icon);
        imageView2.setImageResource(imgicon[i]);
        return view;
    }
}
