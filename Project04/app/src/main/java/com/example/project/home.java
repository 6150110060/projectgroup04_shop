package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        setTitle("");


        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),pointsystem.class));
                overridePendingTransition(0,0);
            }
        });

        Intent i = getIntent();
        String idshop = i.getStringExtra("iduser");

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.home);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:
                        break;

                    case R.id.discount:
                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:
                        startActivity(new Intent(getApplicationContext(),pointsystem.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.icon4:
                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.option:
                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });
    }

}