package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class Optionmain extends AppCompatActivity {

    Button button,button2;
    String[] option = {"โปรไฟล์","แก้ไขรหัสผ่าน"};
    int[] imgoption = {R.drawable.ic_baseline_account_box_24,R.drawable.ic_baseline_lock_24};
    int[] imgnext = {R.drawable.ic_baseline_arrow_forward_ios_24,R.drawable.ic_baseline_arrow_forward_ios_24};
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optionmain);

        listView = findViewById(R.id.listoption);

        Optioncustom optionadther = new Optioncustom(getApplicationContext(),option,imgoption,imgnext);
        listView.setAdapter(optionadther);

        setTitle("อื่นๆ");

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
if (i==0){
    startActivity(new Intent(getApplicationContext(),EditProfile.class));
    overridePendingTransition(0,0);
}
                if (i==1){
                    startActivity(new Intent(getApplicationContext(),Editpassword.class));
                    overridePendingTransition(0,0);
                }

            }
        });

        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),pointsystem.class));
                overridePendingTransition(0,0);
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.option);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:

                        startActivity(new Intent(getApplicationContext(),pointsystem.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.icon4:

                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.option:


                        break;
                }
                return true;
            }
        });

    }

    public void out(View view){

        startActivity(new Intent(Optionmain.this,login.class));
        finish();
//        SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
//        SharedPreferences.Editor edit = preferences.edit();
//        edit.clear();
//        edit.commit();

    }
}