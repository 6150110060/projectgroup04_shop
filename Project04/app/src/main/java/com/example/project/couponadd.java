package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;

public class couponadd extends AppCompatActivity {

    String idshop = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_couponadd);

        setTitle("เพิ่มส่วนลด");

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.discount);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:

                        startActivity(new Intent(getApplicationContext(),coupons.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.point:

                        startActivity(new Intent(getApplicationContext(),pointsystem.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.icon4:

                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.option:

                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });
        TabItem tabItem = findViewById(R.id.item1);

        TabLayout tabLayout = findViewById(R.id.discountcoupon);
        tabLayout.setTabGravity(tabLayout.GRAVITY_FILL);
        PagerAdapter2 pagerAdapter2 = new PagerAdapter2(getSupportFragmentManager(),tabLayout.getTabCount());
        final ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(pagerAdapter2);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton floatingActionButton = findViewById(R.id.floatingActionButton);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),pointsystem.class));
                overridePendingTransition(0,0);
            }
        });

        }

        public void idshop(){
            SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
            idshop = preferences.getString("iduser2", "14");
    }
}