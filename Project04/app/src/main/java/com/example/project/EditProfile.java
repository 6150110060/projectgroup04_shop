package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfile extends AppCompatActivity {

    EditText name,shopname,phone,email,addres;
    String iduser;
    String idshop = null;


    Menu menu_edit;
    Systeminterface systeminterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.myBASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);

idshop();

        name = findViewById(R.id.Editname);
        shopname = findViewById(R.id.Editnameshop);
        phone = findViewById(R.id.Editphone);
        email = findViewById(R.id.Editmail);
        addres = findViewById(R.id.Editaddressshop);
        setTitle("โปรไฟล์");

        name.setFocusableInTouchMode(false);
        shopname.setFocusableInTouchMode(false);
        phone.setFocusableInTouchMode(false);
        email.setFocusableInTouchMode(false);
        addres.setFocusableInTouchMode(false);

        getuser();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editprofit,menu);
        menu_edit = menu;
        menu_edit.findItem(R.id.save).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.edit:
                name.setFocusableInTouchMode(true);
                shopname.setFocusableInTouchMode(true);
                phone.setFocusableInTouchMode(true);
                email.setFocusableInTouchMode(true);
                addres.setFocusableInTouchMode(true);
                menu_edit.findItem(R.id.edit).setVisible(false);
                menu_edit.findItem(R.id.save).setVisible(true);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.showSoftInput(name,InputMethodManager.SHOW_IMPLICIT);
                setTitle("แก้ไขโปรไฟล์");




                return true;
            case R.id.save:
                setTitle("โปรไฟล์");
                boolean i = false;
                name.setFocusableInTouchMode(i);
                shopname.setFocusableInTouchMode(i);
                phone.setFocusableInTouchMode(i);
                email.setFocusableInTouchMode(i);
                addres.setFocusableInTouchMode(i);
                name.setFocusable(i);
                shopname.setFocusable(i);
                phone.setFocusable(i);
                email.setFocusable(i);
                addres.setFocusable(i);
                menu_edit.findItem(R.id.edit).setVisible(true);
                menu_edit.findItem(R.id.save).setVisible(false);
                Toast.makeText(getApplicationContext(), "save surcess", Toast.LENGTH_SHORT).show();




                saveprofile();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void saveprofile(){

        Call<loginresponse> call = systeminterface.editprofile(idshop,
                name.getText().toString(),
                shopname.getText().toString(),
                phone.getText().toString(),
                email.getText().toString(),
                addres.getText().toString());
        call.enqueue(new Callback<loginresponse>() {
            @Override
            public void onResponse(Call<loginresponse> call, Response<loginresponse> response) {

                Toast.makeText(getApplicationContext(), "edit", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<loginresponse> call, Throwable t) {

            }
        });
    }

    public void getuser(){
        Call<List<loginresponse>> call = systeminterface.viewiduser(idshop);
        call.enqueue(new Callback<List<loginresponse>>() {
            @Override
            public void onResponse(Call<List<loginresponse>> call, Response<List<loginresponse>> response) {
                name.setText(response.body().get(0).getName());
                shopname.setText(response.body().get(0).getShopname());
                phone.setText(response.body().get(0).getPhonenumber());
                email.setText(response.body().get(0).getEmail());
                addres.setText(response.body().get(0).getAddress());

            }

            @Override
            public void onFailure(Call<List<loginresponse>> call, Throwable t) {

            }
        });
    }

    public void idshop(){
        SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
        idshop = preferences.getString("iduser2", "14");


    }
}