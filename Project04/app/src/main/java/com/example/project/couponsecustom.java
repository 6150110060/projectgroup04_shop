package com.example.project;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

public class couponsecustom extends BaseAdapter {


    private Context context;

    
    private ArrayList<Integer> imgcouponse;
    
    private LayoutInflater layoutInflater;


    public couponsecustom(Context applicationContext, ArrayList<Integer> imgcouponse) {
        context = applicationContext;
        
        this.imgcouponse = imgcouponse;
        
    }

    @Override
    public int getCount() {
        return imgcouponse.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (layoutInflater == null){
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        if (view == null){
            view = layoutInflater.inflate(R.layout.listcouponse,null);
        }

        ImageView imageView = view.findViewById(R.id.couponse);
        

        imageView.setImageResource(imgcouponse.get(i));
        

        
        return view;
    }
}
