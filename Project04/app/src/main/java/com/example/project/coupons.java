package com.example.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class coupons extends AppCompatActivity {


int couponse10 = R.drawable.coupon2;
    int[] imgcouponse = {R.drawable.couponn1,R.drawable.coupon2,R.drawable.coupon3,R.drawable.coupon4,R.drawable.testcoupon3};

    ListView listView;

    ArrayList<Integer> arrayList;
    Systeminterface systeminterface;

    String idshop = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);

        listView = findViewById(R.id.listview);
        arrayList = new ArrayList<Integer>();

        couponsecustom couponsecustom = new couponsecustom(getApplicationContext(),arrayList);

        listView.setAdapter(couponsecustom);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(systeminterface.myBASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        systeminterface = retrofit.create(Systeminterface.class);

        FloatingActionButton floatingActionButton = findViewById(R.id.iconadd);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),couponadd.class));
                overridePendingTransition(0,0);
            }
        });

        FloatingActionButton floatingActionButton2 = findViewById(R.id.iconedit);
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),editcoupon.class));
                overridePendingTransition(0,0);
            }
        });

        setTitle("ส่วนลดร้านค้า");

        viewcouponshop();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavigationView);
        bottomNavigationView.setSelectedItemId(R.id.discount);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:

                        startActivity(new Intent(getApplicationContext(),home.class));
                        overridePendingTransition(0,0);
                        break;

                    case R.id.discount:


                        break;

                    case R.id.point:

                        startActivity(new Intent(getApplicationContext(),pointsystem.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.icon4:

                        startActivity(new Intent(getApplicationContext(),customer.class));
                        overridePendingTransition(0,0);
                        break;
                    case R.id.option:

                        startActivity(new Intent(getApplicationContext(),Optionmain.class));
                        overridePendingTransition(0,0);
                        break;
                }
                return true;
            }
        });
    }

    public void viewcouponshop(){
        idshop();
        Call<List<couponresponse>> call = systeminterface.viewcouponshop(idshop);
        call.enqueue(new Callback<List<couponresponse>>() {
            @Override
            public void onResponse(Call<List<couponresponse>> call, Response<List<couponresponse>> response) {

if (!arrayList.isEmpty()) {
    for (int i = 0; i < response.body().size(); i++) {
        int ii = Integer.parseInt(response.body().get(i).getIdcoupon());
        ii = ii - 1;
        arrayList.add(imgcouponse[ii]);
    }
}
else {

}
            }

            @Override
            public void onFailure(Call<List<couponresponse>> call, Throwable t) {

            }
        });
    }

    public void idshop(){
        SharedPreferences preferences = getSharedPreferences("MY_PREFS", Context.MODE_PRIVATE);
        idshop = preferences.getString("iduser2", "14");


    }
}